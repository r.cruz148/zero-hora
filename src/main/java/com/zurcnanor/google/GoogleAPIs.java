package com.zurcnanor.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.drive.model.*;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.api.services.drive.Drive;
import java.util.List;

/**
 * 
 * @author Ronan Cruz
 *
 */
public class GoogleAPIs {
	
	/** Application name. */
    private static final String APPLICATION_NAME = "ZeroHora";

    /**
     * Build and return an authorized Drive client service.
     * @return an authorized Drive client service
     * @throws Exception 
     */
    public static Drive getDriveService() throws Exception {
        Credential credential = OAuth2Service.authorize();
        return new Drive.Builder(
                OAuth2Service.HTTP_TRANSPORT, OAuth2Service.JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }
    
    public static SpreadsheetService getSpreadSheetService() throws Exception {
    	Credential credential = OAuth2Service.authorize();
    	SpreadsheetService spreadsheetService = new SpreadsheetService(APPLICATION_NAME);
    	spreadsheetService.setOAuth2Credentials(credential);
    	return spreadsheetService;
    }
    
}

package com.zurcnanor.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.zurcnanor.google.GoogleAPIs;

/**
 * 
 * @author Ronan Cruz
 *
 */
public class GoogleDriveService {
	
	public static final String DRIVE_FOLDER_NAME = "ZeroHora";
	
	private Drive service;
	private File workFolder;

	public GoogleDriveService() throws Exception {
		super();
		this.service = GoogleAPIs.getDriveService();
		loadWorkFolder();
	}
	
	private FileList executeQuery(String q) {
		FileList result = null;
		try {
			result = this.service.files().list().setQ(q).execute();
		} catch (IOException e) {
			System.err.println("Erro ao efetuar busca: " + e);;
		}
		return result;
	}
	
	public File insertFile(String title, String description, String mimeType) {
		File file = new File();
		file.setTitle(title);
		file.setDescription(description);
		file.setMimeType(mimeType);
		if (this.workFolder != null) {
			List<ParentReference> parents = new ArrayList<ParentReference>();
			parents.add(new ParentReference()
					.setKind(this.workFolder.getKind())
					.setId(this.workFolder.getId()));
			file.setParents(parents);
		}
		
		try {
			file = this.service.files().insert(file).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("File ID: " + file.getId());
		
		return file;
	}
	
	public File insertSpreadSheet(String title, String description) {
		return insertFile(title, description, "application/vnd.google-apps.spreadsheet");
	}
	
	public File insertFolder(String title, String description) {
		return insertFile(title, description, "application/vnd.google-apps.folder");
	}
	
	public File findFile(String title, String mimeType) {
		FileList list = executeQuery("title = '" + title + "' and mimeType = '" + mimeType + "'");
		if (list.getItems().isEmpty()) {
			return null;
		} else {
			return list.getItems().get(0);
		}
	}
	
	public File findSpreadsheet(String title) {
		return findFile(title, "application/vnd.google-apps.spreadsheet");
	}
	
	public File findFolder(String title) {
		return findFile(title, "application/vnd.google-apps.folder");
	}
	
	private void loadWorkFolder() {
		this.workFolder = findFolder(DRIVE_FOLDER_NAME);
		if (this.workFolder == null)
			this.workFolder = insertFolder(DRIVE_FOLDER_NAME, "");
		System.out.printf("Pasta '%s' selecionada\n", this.workFolder.getTitle());
	}
}

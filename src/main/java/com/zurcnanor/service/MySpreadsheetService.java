package com.zurcnanor.service;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.google.api.client.json.webtoken.JsonWebSignature.Header;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import com.zurcnanor.google.GoogleAPIs;

public class MySpreadsheetService {

	private static final String PREFIX_SPREADSHEET_NAME = "Registro de Ponto - ";
	private static final int COL_COUNT_DEFAULT = 12;
	private static final int ROW_COUNT_DEFAULT = 1;
	private static final String[] HEADER_DEFAULT = {"Data", "Dia", "Ent1", "Sai1", "Ent2", "Sai2", "Ent3", "Sai3", "Ent4", "Sai4", "Saldo"};
	private static final String SPREADSHEET_FEED_URL = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";	
	
	private SpreadsheetService service;
	private GoogleDriveService drive;
	private SpreadsheetEntry spreadsheet;
	private WorksheetEntry worksheet;
	private Calendar calendar;
	
	private URL spreadsheetFeedURL;
	
	public MySpreadsheetService() throws Exception {
		super();
		this.service = GoogleAPIs.getSpreadSheetService();
		this.drive = new GoogleDriveService();
		this.spreadsheetFeedURL = new URL(SPREADSHEET_FEED_URL);
		this.calendar = new GregorianCalendar();
		
		loadSpreadsheet();
		
		/*ListEntry row = new ListEntry();
		row.getCustomElements().setValueLocal("firstname", "Joe");
	    row.getCustomElements().setValueLocal("lastname", "Smith");
	    row.getCustomElements().setValueLocal("age", "26");
	    row.getCustomElements().setValueLocal("height", "176");
	    
	    row = service.insert(this.worksheet.getListFeedUrl(), row);*/
	}
	
	private String getNameSpreadsheet() {
		int year = calendar.get(Calendar.YEAR);
		return PREFIX_SPREADSHEET_NAME + year;
	}
	
	private String getNameWorksheet() {
		String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
		return month;
	}
	
	private void checkSpreadsheet(String title) {
		if (this.drive.findSpreadsheet(title) == null) {
			this.drive.insertSpreadSheet(title, "");
		}
	}
	
	private void loadSpreadsheet() {
		checkSpreadsheet(getNameSpreadsheet());
		this.spreadsheet = selectSpreadsheet(getNameSpreadsheet());
		this.worksheet = selectWorksheet(getNameWorksheet());
		if (worksheet == null)
			createWorksheet(getNameWorksheet());
	}
	
	public SpreadsheetEntry selectSpreadsheet(String title) {
		try {
			SpreadsheetFeed feed = service.getFeed(spreadsheetFeedURL, SpreadsheetFeed.class);
			
			for (SpreadsheetEntry spreadsheet : feed.getEntries()) {
				if (spreadsheet.getTitle().getPlainText().equals(getNameSpreadsheet()))
					return spreadsheet;
			}
		} catch (IOException | ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public WorksheetEntry selectWorksheet(String title) {
		try {
			for (WorksheetEntry worksheet : this.spreadsheet.getWorksheets()) {
				String nameWE = worksheet.getTitle().getPlainText();
				if (nameWE.equals(title))
					return worksheet;
			}
		} catch (IOException | ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void createWorksheet(String title) {
		WorksheetEntry worksheet = new WorksheetEntry();
		worksheet.setTitle(new PlainTextConstruct(title));
		worksheet.setColCount(COL_COUNT_DEFAULT);
		worksheet.setRowCount(ROW_COUNT_DEFAULT);
		
		try {
			this.worksheet = service.insert(this.spreadsheet.getWorksheetFeedUrl(), worksheet);
			createDefaultHeader();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		generateDaysOfMonth();
	}
	
	private void createDefaultHeader() throws IOException, ServiceException {
		CellFeed feed = service.getFeed(this.worksheet.getCellFeedUrl(), CellFeed.class);
		for (int i = 0; i < HEADER_DEFAULT.length; i++) {
			feed.insert(new CellEntry(1, i + 1, HEADER_DEFAULT[i]));
		}
	}
	
	private void generateDaysOfMonth() {
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		Calendar c = new GregorianCalendar(year, month, 1);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(c.getTimeZone());
		ListEntry row = null;
	
		while (c.get(Calendar.MONTH) == month) {
			String date = dateFormat.format(c.getTime());
			String dayOfWeek = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
			
			row = new ListEntry();
			row.getCustomElements().setValueLocal(HEADER_DEFAULT[0], date);
		    row.getCustomElements().setValueLocal(HEADER_DEFAULT[1], dayOfWeek);
		    
		    try {
				row = service.insert(this.worksheet.getListFeedUrl(), row);
			} catch (IOException | ServiceException e) {
				e.printStackTrace();
			}
			c.add(Calendar.DAY_OF_MONTH, 1);
		}
	}
	
}